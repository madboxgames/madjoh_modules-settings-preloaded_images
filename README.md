# preloaded_images settings #

v1.0.2

Here is defined a list of generic images to preload for your application.

You should add here every image you are using in your application in order to load them during the loading animation.