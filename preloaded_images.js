var dependencies = {
	madjoh_modules : [
		'styling',
		'gallery'
	]
};
var list = MadJohRequire.getList(dependencies);
define(list, function(require, Styling, Gallery){
	var PreloadedImages = [];

	// GENERIQUES GLOBAL
		PreloadedImages.push('madjoh_modules/gallery/'+Styling.deviceSize+'/btn_facebook.png');
		PreloadedImages.push('madjoh_modules/gallery/'+Styling.deviceSize+'/asset_facebook_small.png');

		PreloadedImages.push('madjoh_modules/gallery/'+Styling.deviceSize+'/btn_madjoh.png');
		PreloadedImages.push('madjoh_modules/gallery/'+Styling.deviceSize+'/asset_madjoh_small.png');
		PreloadedImages.push('madjoh_modules/gallery/'+Styling.deviceSize+'/asset_madjoh_create_small.png');
		PreloadedImages.push('madjoh_modules/gallery/'+Styling.deviceSize+'/asset_offline_small.png');

		PreloadedImages.push('madjoh_modules/gallery/'+Styling.deviceSize+'/ajax_loading.gif');

	return PreloadedImages;
});